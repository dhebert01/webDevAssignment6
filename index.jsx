"use strict";
//https://db.ygoprodeck.com/api/v7/cardinfo.php?fname= this is my fetch url
const { useState } = React;

function Search({myValue, setMyValueHandler}){
  return <input type="text" value={myValue} onInput={(e)=> setMyValueHandler(e.target.value)}></input>
}

function Submit({myValue, setCardList}){
  return <button onClick={(e)=> {e.preventDefault(); fetchInfo({myValue, setCardList});}}>Search</button>
}

function fetchInfo({myValue, setCardList}){
  fetch(`https://db.ygoprodeck.com/api/v7/cardinfo.php?fname=${myValue}`, {})
  .then(response => response.json())

  .then((obj) => {
    if (obj.data && obj.data.length > 0) {
      setCardList(obj.data);
    } else {
      setCardList([]);
    }
  })

  .catch(error => console.error(error))
}

function DisplayInfo({myCard}){
  return <main>
    <h2>{myCard.name}</h2>
    <img src={myCard.card_images[0].image_url} alt={myCard.name}/>
    <p>Race: {myCard.race}</p>
    <p>{myCard.desc}</p>
    <p>Attack: {myCard.atk}</p>
    <p>Defence: {myCard.def}</p>
    <p>Price on card market: ${myCard.card_prices[0].cardmarket_price}</p>
  </main>
}

let runOnce = true;
function App() {
  const startingCard = {
    name: 'Loading',
    type: 'Card Type',
    desc: 'Card Description',
    atk: 0,
    def: 0,
    level: 0,
    race: 'Card Race',
    attribute: 'Card Attribute',
    card_images: [
      {
        image_url: 'https://placehold.it/200x300', // Replace with a placeholder image URL or your default image
      },
    ],
    card_prices: [
      {
        cardmarket_price: '0.00',
      },
    ],
  };

  function getRandomCard(){
    while(runOnce){
      fetch(`https://db.ygoprodeck.com/api/v7/randomcard.php`, {})
      .then(response => response.json())
    
      .then((obj) => {
        if (obj) {
          setMyCard(obj);
        } else {
          setMyCard([]);
        }
      })
    
      .catch(error => console.error(error))
      runOnce = false;
    }
  }
  getRandomCard();
  const [myValue, setMyValue] = useState("Search card here!!");
  const [myCard, setMyCard] = useState(startingCard);
  const [cardList, setCardList] = useState([]);
  return <div id="wrapper">
    <header>
      <h1>Yu-Gi-Oh Info Search</h1>
      <form>
        <div>
          <Search myValue={myValue} setMyValueHandler={setMyValue}/>
          <Submit myValue={myValue} setCardList={setCardList}/>
        </div>
        <ul>
          <div>
            {cardList.map((card) => <li onClick={(e) => {setMyCard(card)}}>{card.name}</li>)}
          </div>
        </ul>
      </form>
    </header>
    <DisplayInfo myCard={myCard}/>
  </div>
 }
 ReactDOM.render( <App />, document.body );